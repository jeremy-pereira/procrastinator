//
//  ProcrastinatorKitTests.swift
//  ProcrastinatorKitTests
//
//  Created by Jeremy Pereira on 01/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
import Foundation
@testable import ProcrastinatorKit

class ProcrastinatorKitTests: XCTestCase
{
	fileprivate static let helper = PKTestHelper(testFolder: "ProcrastinatorKitTests")

	override class func setUp()
	{
		super.setUp()
		helper.reset()
	}

	func testCreateDatabase()
	{
		let dbName = "testCreateDatabase.procrastinator"
		do
		{
			let db = try Database(path: ProcrastinatorKitTests.helper.makePath(dbName))
			XCTAssert(Database.expectedVersion == db.version, "Expected version \(Database.expectedVersion), got \(db.version)")
			let connection = try ProcrastinatorKitTests.helper.createConnection(name: dbName)
			if let timeline = try connection.table(name: "timeline")
			{
				XCTAssert(timeline.id != nil)
			}
			else
			{
				XCTFail("no timeline")
			}
			if let task = try connection.table(name: "task")
			{
				XCTAssert(task.id != nil)
				XCTAssert(task.shortDescription != nil)
				XCTAssert(task.duration != nil)
			}
			else
			{
				XCTFail("no task")
			}
			if let recipe = try connection.table(name: "recipe")
			{
				XCTAssert(recipe.id != nil)
			}
			else
			{
				XCTFail("no recipe")
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	// The test below is just a representation of a model to help us decide
	// what to build.
	// Based on this recipe:
	// https://www.bbcgoodfood.com/recipes/slow-cooker-beef-bourguignon
	func testBoeufBourginon()
	{

//		1½ kg stewing or braising steak, cut into small chunks
//		2 large onions
//		, chopped
//		2 carrots, chopped
//		2 celery stalks, chopped
//		2 bay leaves
//		2 thyme sprigs or rosemary sprigs
//		3 tbsp plain flour
//		750ml bottle red wine
//		1 beef stock cube
//		1 tsp caster sugar
//		2 tbsp tomato purée
//		100g unsmoked bacon lardons
//		6 small shallots or baby onions, halved or quartered
//		300g closed cup mushrooms, halved or quartered
//		mashed potatoes or crusty bread, to serve

		let date = Date()
		let timeline = TimeLine()
		let recipe = Recipe()
		let getOil = Task("get the oil") //	3 tbsp vegetable oil
		getOil.duration = 300
		let heatOil = Task("heat oil") // Turn the slow cooker to low and heat 2 tbsp of the oil in a large frying pan
		heatOil.duration = 60
		//		Season the meat and fry for 3-4 mins in batches until browned all over, scooping out each batch with a slotted spoon and transferring the browned meat to a plate.
		//		Tip the onion, carrot and celery into the pan and fry for 5-10 mins until soft. Add the herbs and flour and cook for another 2 mins. Pour a splash of the wine into a bowl, then add the stock cube, sugar and tomato purée and mix to form a paste. Scrape the paste into the onion mix and pour in the remaining wine. Bring the mixture to a bubble, then transfer to the slow cooker. Stir in the browned beef, topping up with a splash of water to cover the meat if needed. Simmer on low for 6-8 hrs until the meat is falling apart but still holding its shape.
		//		About 35 mins before serving, heat the remaining oil in a pan. Fry the bacon, shallots and mushrooms for 5-8 mins until caramelised and the veg is starting to soften, then tip into the slow cooker. Simmer the stew gently on high for 30 mins. If you want a thicker gravy,


		timeline.add(recipe, endingAt: date + 3600)

    }
}
