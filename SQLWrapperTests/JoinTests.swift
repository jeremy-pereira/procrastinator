//
//  JoinTests.swift
//  SQLWrapperTests
//
//  Created by Jeremy Pereira on 10/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import SQLWrapper

class JoinTests: XCTestCase
{
	fileprivate static let helper = TestHelper(testFolder: "JoinTests")

	override class func setUp()
	{
		helper.reset()
	}

    func testInnerJoin()
	{
		let db = createExampleDB(name: "testInnerJoin.sqlite3")
		let band = try! db.table(name: "band")!
		let person = try! db.table(name: "person")!
		let sql = Select([band.name!, person.name!], from: band.innerJoin(person, on: band.id! == person.band!))
		do
		{
			let result = try db.query(sql)
			var count = 0
			for row in result
			{
				let bandName: String = try row.value(column: band.name!)
				XCTAssert(["The Beatles", "Pink Floyd"].contains(bandName), "Wrong band name \(bandName)")
				count += 1
			}
			XCTAssert(count == 9)
		}
		catch
		{
			XCTFail("\(error)")
		}
    }

	func testLeftJoin()
	{
		let db = createExampleDB(name: "testLeftJoin.sqlite3")
		let band = try! db.table(name: "band")!
		let person = try! db.table(name: "person")!
		let sql = Select([band.name!, person.name!], from: person.leftJoin(band, on: band.id! == person.band!))
		do
		{
			let result = try db.query(sql)
			var count = 0
			for row in result
			{
				let bandName: String? = try row.nullableValue(column: band.name!)
				if let bandName = bandName
				{
					XCTAssert(["The Beatles", "Pink Floyd"].contains(bandName), "Wrong band name \(bandName)")
				}
				else
				{
					let name: String = try row.value(column: person.name!)
					XCTAssert(name == "Jeremy")
				}
				count += 1
			}
			XCTAssert(count == 10)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func createExampleDB(name: String) -> Connection
	{
		return JoinTests.helper.createExampleDB(name: name, populate: {
			db in
			let band = Table(name: "band", columns: [
				Column(name: "id", type: .integer, primaryKey: true, nullable: false),
				Column(name: "name", type: .text, nullable: false)
				])
			try db.create(table: band)
			try db.insert(into: band, values: [band.id! <- 1, band.name! <- "The Beatles"])
			try db.insert(into: band, values: [band.id! <- 2, band.name! <- "Pink Floyd"])

			let person = Table(
				name: "person",
				columns: [
					Column(name: "id", type: .integer, primaryKey: true, nullable: false),
					Column(name: "name", type: .text),
					Column(name: "familyname", type: .text),
					Column(name: "band", type: .integer),
					]
			)
			let id = person.id!
			let name = person.name!
			let familyName = person.familyname!

			try db.create(table: person)
			try db.insert(into: person, values: [id <- 1, name <- "John", familyName <- "Lennon", person.band! <- 1])
			try db.insert(into: person, values: [id <- 2, name <- "Paul", familyName <- "McCartney", person.band! <- 1])
			try db.insert(into: person, values: [id <- 3, name <- "George", familyName <- "Harrison", person.band! <- 1])
			try db.insert(into: person, values: [id <- 4, name <- "Ringo", familyName <- "Starr", person.band! <- 1])
			try db.insert(into: person, values: [id <- 5, name <- "Roger", familyName <- "Waters", person.band! <- 2])
			try db.insert(into: person, values: [id <- 6, name <- "Rick", familyName <- "Wright", person.band! <- 2])
			try db.insert(into: person, values: [id <- 7, name <- "David", familyName <- "Gilmour", person.band! <- 2])
			try db.insert(into: person, values: [id <- 8, name <- "Nick", familyName <- "Mason", person.band! <- 2])
			try db.insert(into: person, values: [id <- 9, name <- "Syd", familyName <- "Barrett", person.band! <- 2])
			try db.insert(into: person, values: [id <- 10, name <- "Jeremy", familyName <- "Pereira"])
		})
	}

}
