//
//  TableTests.swift
//  SQLWrapperTests
//
//  Created by Jeremy Pereira on 03/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import SQLWrapper

class TableTests: XCTestCase
{
	fileprivate static let helper = TestHelper(testFolder: "TableTests")

	override class func setUp()
	{
		helper.reset()
	}

    func testCreateTable()
	{
		let db: Connection = TableTests.helper.createConnectionOrFatal(name: "testCreateTable.sqlite3")
		defer { db.close() }

		let contact = Table(
			name: "contact",
			columns: [
				Column(name: "id", type: .integer, primaryKey: true, nullable: false),
				Column(name: "name", type: .text)
			]
		)
		do
		{
			try db.create(table: contact)
		}
		catch
		{
			XCTFail("\(error)")
		}
    }


	func testCreateTableUnique()
	{
		let db: Connection = TableTests.helper.createConnectionOrFatal(name: "testCreateTableUnique.sqlite3")
		defer { db.close() }

		let contact = Table(
			name: "contact",
			columns: [
				Column(name: "id", type: .integer, primaryKey: true, nullable: false),
				Column(name: "name", type: .text, unique: true)
			]
		)
		do
		{
			try db.create(table: contact)
			try db.insert(into: contact, values: [contact.id! <- 1, contact.name! <- "jeremy"])
		}
		catch
		{
			fatalError("\(error)")
		}
		do
		{
			try db.insert(into: contact, values: [contact.id! <- 2, contact.name! <- "jeremy"])
			XCTFail("Should have failed on unique constraint")
		}
		catch
		{
			print("\(error)")
		}
	}

	func testGetTableFromDb()
	{
		let db = createExampleDB(name: "testGetTableFromDb.sqlite3")
		defer { db.close() }
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Failed to find the contact table") ; return }
			XCTAssert(contact.columns.count == 4)
			guard let nameColumn = contact.name
				else { XCTFail("No name column") ; return }
			XCTAssert(nameColumn.name == "name", "is not using dynamic lookup")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testGetNonExistentTableFromDb()
	{
		let db = createExampleDB(name: "testGetNonExistentTableFromDb.sqlite3")
		defer { db.close() }
		do
		{
			let tableName = "vfdsafdf"
			guard try db.table(name: tableName) == nil
				else { XCTFail("Managed to find a table") ; return }
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testInsert()
	{
		let db = createExampleDB(name: "testInsert.sqlite3")
		defer { db.close() }
		let contact = try! db.table(name: "contact")!

		let id = contact.id!
		let name = contact.name!

		do
		{
			let rows = try db.insert(into: contact, values: [id <- 1, name <- "jeremy"])
			XCTAssert(rows == 1)
		}
		catch
		{
			XCTFail("\(error.description)")
		}
		do
		{
			try db.insert(into: contact, values: [id <- 1, name <- "jeremy"])
			XCTFail("Should not be able to insert twice with the same primary key")
		}
		catch
		{
			print("\(error.description)")
		}
	}

	func testUpdate()
	{
		let db = createPopulatedExampleDB(name: "testUpdate.sqlite3")
		let contact = try! db.table(name: "contact")!
		let name = contact.name!
		let familyName = contact.familyname!

		do
		{
			let count = try db.update(contact, set: [name <- "muddy"], where: familyName == "waters")
			XCTAssert(count == 1)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testUpdateToNull()
	{
		let db = createPopulatedExampleDB(name: "testUpdateToNull.sqlite3")
		let contact = try! db.table(name: "contact")!
		let name = contact.name!
		let familyName = contact.familyname!

		do
		{
			let count = try db.update(contact, set: [name <- null], where: familyName == "waters")
			XCTAssert(count == 1)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	func testUpdateNullString()
	{
		let db = createPopulatedExampleDB(name: "testUpdateNullString.sqlite3")
		let contact = try! db.table(name: "contact")!
		let name = contact.name!
		let familyName = contact.familyname!

		do
		{
			let newValue: String? = nil
			let count = try db.update(contact, set: [name <- newValue], where: familyName == "waters")
			XCTAssert(count == 1)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	func testDelete()
	{
		let db = createPopulatedExampleDB(name: "testDelete.sqlite3")
		let contact = try! db.table(name: "contact")!
		let familyName = contact.familyname!

		do
		{
			let count = try db.delete(from: contact, where: familyName == "pereira")
			XCTAssert(count == 2)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func createExampleDB(name: String) -> Connection
	{
		return TableTests.helper.createExampleDB(name: name, populate: {
			db in
			let contact = Table(
				name: "contact",
				columns: [
					Column(name: "id", type: .integer, primaryKey: true, nullable: false),
					Column(name: "name", type: .text),
					Column(name: "sampleBlob", type: .blob),
					Column(name: "sampleReal", type: .real)
				]
			)
			try db.create(table: contact)
		})
	}

	func createPopulatedExampleDB(name: String) -> Connection
	{
		return TableTests.helper.createExampleDB(name: name, populate: {
			db in
			let contact = Table(
				name: "contact",
				columns: [
					Column(name: "id", type: .integer, primaryKey: true, nullable: false),
					Column(name: "name", type: .text),
					Column(name: "familyname", type: .text),
					]
			)
			let id = contact.column("id")!
			let name = contact.column("name")!
			let familyName = contact.column("familyname")!

			try db.create(table: contact)
			try db.insert(into: contact, values: [id <- 1, name <- "jeremy", familyName <- "pereira"])
			try db.insert(into: contact, values: [id <- 2, name <- "richard", familyName <- "pereira"])
			try db.insert(into: contact, values: [id <- 3, name <- "roger", familyName <- "waters"])
		})
	}

}
