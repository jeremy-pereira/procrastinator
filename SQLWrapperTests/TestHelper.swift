//
//  TestHelper.swift
//  SQLWrapperTests
//
//  Created by Jeremy Pereira on 06/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import SQLWrapper

struct TestHelper
{
	/// The foldfer inside which test databases reside
	let testFolder: String

	init(testFolder: String)
	{
		self.testFolder = testFolder
	}


	/// Resets the test folder for a clean run
	func reset()
	{
		let fm = FileManager.default
		print("Running at \(fm.currentDirectoryPath)")

		if fm.fileExists(atPath: testFolder)
		{
			try! fm.removeItem(atPath: testFolder)
		}
		try! fm.createDirectory(atPath: testFolder, withIntermediateDirectories: false, attributes: nil)
	}


	/// Create a new database connection
	///
	/// The database for which we create a connection is located in `testFolder`
	/// - Parameter name: Name of the database to attach to
	/// - Returns: A database connection.
	func createConnectionOrFatal(name: String) -> Connection
	{
		do
		{
			let db = try SQLite3.open(path: testFolder + "/" + name)
			return db
		}
		catch
		{
			fatalError("Failed to create a database: \(error)")
		}
	}

	/// Create an example database with some populated tables.
	///
	/// The connection is created in the folder pointed at by `tableTestFolder`.
	///
	/// If something goes wrong, this is considered a fatal error because
	/// creation of the database should always work here.
	///
	/// - Parameters:
	///   - name: Name of the database to create
	///   - populate: Closure that creates tables etc and populates them
	/// - Returns: A connection to the database.

	func createExampleDB(name: String, populate: (Connection) throws -> ()) -> Connection
	{
		let db = createConnectionOrFatal(name: name)
		do
		{
			try populate(db)
		}
		catch
		{
			fatalError("\(error)")
		}
		return db
	}

}
