//
//  SQLWrapperTests.swift
//  SQLWrapperTests
//
//  Created by Jeremy Pereira on 02/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import SQLWrapper

class SQLWrapperTests: XCTestCase
{
	let testDBName = "testdb.sqlite3"
    override func setUp()
	{
		let fm = FileManager.default
        print("Running at \(fm.currentDirectoryPath)")
		if fm.fileExists(atPath: testDBName)
		{
			try! fm.removeItem(atPath: testDBName)
		}
    }

    override func tearDown()
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testOpenDB()
	{
		do
		{
			let connection = try SQLite3.open(path: testDBName)
			connection.close()
		}
		catch
		{
			XCTFail("\(error)")
		}
    }

}
