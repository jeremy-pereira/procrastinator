//
//  TransactionTests.swift
//  SQLWrapperTests
//
//  Created by Jeremy Pereira on 14/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import SQLWrapper

class TransactionTests: XCTestCase
{
	fileprivate static let helper = TestHelper(testFolder: "TransactionTests")

	override class func setUp()
	{
		helper.reset()
	}

    func testNormalCommit()
	{
		let db = createExampleDB(name: "testNormalCommit")
		do
		{
			let person = try db.table(name: "person")!
			let id = person.id!
			let name = person.name!
			let familyName = person.familyname!

			try db.withTransaction
			{
				try db.insert(into: person, values: [id <- 11, name <- "David", familyName <- "Byrne"])
				try db.insert(into: person, values: [id <- 12, name <- "Tina", familyName <- "Weymouth"])
			}
			let sql = Select([id], from: person)
			let count = try db.query(sql).reduce(0)
			{
				runningTotal, _ in runningTotal + 1
			}
			XCTAssert(count == 12)

		}
		catch
		{
			XCTFail("\(error)")
		}
    }

	func testImplicitRollback()
	{
		let db = createExampleDB(name: "testImplicitRollback")
		let person = try! db.table(name: "person")!
		let id = person.id!
		let name = person.name!
		let familyName = person.familyname!

		do
		{
			try db.withTransaction
			{
				try db.insert(into: person, values: [id <- 11, name <- "David", familyName <- "Byrne"])
				try db.insert(into: person, values: [id <- 11, name <- "Tina", familyName <- "Weymouth"])
			}
			XCTFail("Should throw an exception here")
			return
		}
		catch
		{
			print("\(error)")
		}
		do
		{
			let sql = Select([id], from: person)
			let count = try db.query(sql).reduce(0)
			{
				runningTotal, _ in runningTotal + 1
			}
			XCTAssert(count == 10, "Incorrect count \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testExplicitRollback()
	{
		let db = createExampleDB(name: "testExplicitRollback")
		let person = try! db.table(name: "person")!
		let id = person.id!
		let name = person.name!
		let familyName = person.familyname!

		do
		{
			try db.withTransaction
			{
				try db.insert(into: person, values: [id <- 11, name <- "David", familyName <- "Byrne"])
				try db.insert(into: person, values: [id <- 12, name <- "Tina", familyName <- "Weymouth"])
				throw SQLError.abort("Testing")
			}
			XCTFail("Should throw an exception here")
			return
		}
		catch SQLError.abort(let reason)
		{
			print(reason)
		}
		catch
		{
			XCTFail("\(error)")
		}
		do
		{
			let sql = Select([id], from: person)
			let count = try db.query(sql).reduce(0)
			{
				runningTotal, _ in runningTotal + 1
			}
			XCTAssert(count == 10, "Incorrect count \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}



	func createExampleDB(name: String) -> Connection
	{
		return TransactionTests.helper.createExampleDB(name: name, populate: {
			db in
			let band = Table(name: "band", columns: [
				Column(name: "id", type: .integer, primaryKey: true, nullable: false),
				Column(name: "name", type: .text, nullable: false)
				])
			try db.create(table: band)
			try db.insert(into: band, values: [band.id! <- 1, band.name! <- "The Beatles"])
			try db.insert(into: band, values: [band.id! <- 2, band.name! <- "Pink Floyd"])

			let person = Table(
				name: "person",
				columns: [
					Column(name: "id", type: .integer, primaryKey: true, nullable: false, unique: true),
					Column(name: "name", type: .text),
					Column(name: "familyname", type: .text),
					Column(name: "band", type: .integer),
					]
			)
			let id = person.id!
			let name = person.name!
			let familyName = person.familyname!

			try db.create(table: person)
			try db.insert(into: person, values: [id <- 1, name <- "John", familyName <- "Lennon", person.band! <- 1])
			try db.insert(into: person, values: [id <- 2, name <- "Paul", familyName <- "McCartney", person.band! <- 1])
			try db.insert(into: person, values: [id <- 3, name <- "George", familyName <- "Harrison", person.band! <- 1])
			try db.insert(into: person, values: [id <- 4, name <- "Ringo", familyName <- "Starr", person.band! <- 1])
			try db.insert(into: person, values: [id <- 5, name <- "Roger", familyName <- "Waters", person.band! <- 2])
			try db.insert(into: person, values: [id <- 6, name <- "Rick", familyName <- "Wright", person.band! <- 2])
			try db.insert(into: person, values: [id <- 7, name <- "David", familyName <- "Gilmour", person.band! <- 2])
			try db.insert(into: person, values: [id <- 8, name <- "Nick", familyName <- "Mason", person.band! <- 2])
			try db.insert(into: person, values: [id <- 9, name <- "Syd", familyName <- "Barrett", person.band! <- 2])
			try db.insert(into: person, values: [id <- 10, name <- "Jeremy", familyName <- "Pereira"])
		})
	}

}
