//
//  SelectTests.swift
//  SQLWrapperTests
//
//  Created by Jeremy Pereira on 06/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import SQLWrapper

class SelectTests: XCTestCase
{
	fileprivate static let helper = TestHelper(testFolder: "SelectTests")

	override class func setUp()
	{
		super.setUp()
		helper.reset()
	}

    func testSimpleSelect()
	{
		let db = createExampleDB(name: "testSimpleSelect.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
			else { XCTFail("Could not get contaxct table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0
			let select = Select([id, name, familyName], from: contact)
			var queryResult = try db.query(select)
			while let row = try queryResult.nextRow()
			{
				let stringResult: String = try row.value(column: familyName)
				XCTAssert(["pereira", "waters", "nullboy"].contains(stringResult), "Incourrect family name \(stringResult)")
				count += 1
			}
			XCTAssert(count == 4)
		}
		catch
		{
			XCTFail("\(error)")
		}
    }


	func testSimpleSelectSequence()
	{
		let db = createExampleDB(name: "testSimpleSelectSequence.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contact table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0
			let select = Select([id, name, familyName], from: contact)
			let queryResult = try db.query(select)
			for row in queryResult
			{
				let stringResult: String = try row.value(column: familyName)
				XCTAssert(["pereira", "waters", "nullboy"].contains(stringResult), "Incourrect family name \(stringResult)")
				count += 1
			}
			XCTAssert(count == 4)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testSelectNullValues()
	{
		let db = createExampleDB(name: "testSelectNullValues.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contact table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0
			let select = Select([id, name, familyName], from: contact)
			let queryResult = try db.query(select)
			for row in queryResult
			{
				let familyNameString: String = try row.value(column: familyName)
				XCTAssert(["pereira", "waters", "nullboy"].contains(familyNameString), "Incorrect family name \(familyNameString)")
				if let _: String = try row.nullableValue(column: name)
				{
					XCTAssert(familyNameString != "nullboy")
				}
				else
				{
					XCTAssert(familyNameString == "nullboy")
				}

				count += 1
			}
			XCTAssert(count == 4)
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testSelectWithWhere()
	{
		let db = createExampleDB(name: "testSelectWithWhere.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contaxct table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0

			let select = Select([id, name, familyName], from: contact, where: familyName == "pereira")
			var resultSet = try db.query(select)
			while let row = try resultSet.nextRow()
			{
				let nameString: String = try row.value(column: name)
				XCTAssert(["jeremy", "richard"].contains(nameString), "Unexpected name '\(nameString)'")
				count += 1
			}
			XCTAssert(count == 2, "Actual count is \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testSelectWithDistinct()
	{
		let db = createExampleDB(name: "testSelectWithDistinct.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contaxct table") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }

			let select = Select(distinct: true, [familyName], from: contact, where: familyName == "pereira")
			let resultSet = try db.query(select)
			let count = resultSet.reduce(0)
			{
				result, _ in
				result + 1
			}
			XCTAssert(count == 1, "Actual count is \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testSelectWithOr()
	{
		let db = createExampleDB(name: "testSelectWithOr.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contaxct table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0

			let select = Select([id, name, familyName], from: contact, where: name == "jeremy" || name == "richard")
			var resultSet = try db.query(select)
			while let row = try resultSet.nextRow()
			{
				let nameString: String = try row.value(column: name)
				XCTAssert(["jeremy", "richard"].contains(nameString), "Unexpected name '\(nameString)'")
				count += 1
			}
			XCTAssert(count == 2, "Actual count is \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	func testSelectWithAnd()
	{
		let db = createExampleDB(name: "testSelectWithAnd.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contaxct table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0

			let select = Select([id, name, familyName], from: contact, where: name == "jeremy" && name == "richard")
			var resultSet = try db.query(select)
			while let row = try resultSet.nextRow()
			{
				let nameString: String = try row.value(column: name)
				XCTAssert(["jeremy", "richard"].contains(nameString), "Unexpected name '\(nameString)'")
				count += 1
			}
			XCTAssert(count == 0, "Actual count is \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testSelectWithNot()
	{
		let db = createExampleDB(name: "testSelectWithNot.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contaxct table") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0

			let select = Select([familyName], from: contact, where: !(familyName == "pereira" || familyName == "waters"))
			var resultSet = try db.query(select)
			while let row = try resultSet.nextRow()
			{
				let nameString: String = try row.value(column: familyName)
				XCTAssert(nameString == "nullboy", "Unexpected name '\(nameString)'")
				count += 1
			}
			XCTAssert(count == 1, "Actual count is \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testSelectWithNullFilter()
	{
		let db = createExampleDB(name: "testSelectWithNullFilter.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contaxct table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0

			let select = Select([id, name, familyName], from: contact, where: Filter.isNull(name))
			var resultSet = try db.query(select)
			while let row = try resultSet.nextRow()
			{
				let nameString: String = try row.value(column: familyName)
				XCTAssert(nameString == "nullboy", "Unexpected name '\(nameString)'")
				count += 1
			}
			XCTAssert(count == 1, "Actual count is \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}

	func testSelectWithNotNullFilter()
	{
		let db = createExampleDB(name: "testSelectWithNotNullFilter.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contaxct table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0

			let select = Select([id, name, familyName], from: contact, where: !Filter.isNull(name))
			var resultSet = try db.query(select)
			while let row = try resultSet.nextRow()
			{
				let nameString: String = try row.value(column: familyName)
				XCTAssert(nameString != "nullboy", "Unexpected name '\(nameString)'")
				count += 1
			}
			XCTAssert(count == 3, "Actual count is \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	func testSelectOrderBy()
	{
		let db = createExampleDB(name: "testSelectOrderBy.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contaxct table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0

			let select = Select([id, name, familyName], from: contact, where: !Filter.isNull(name), orderBy: [name])
			let resultSet = try db.query(select)
			var expectedNext = ["jeremy", "richard", "roger"].makeIterator()
			for row in resultSet
			{
				let nameString: String = try row.value(column: name)
				let expected = expectedNext.next()!
				XCTAssert(nameString == expected, "Unexpected name '\(nameString)', expected '\(expected)'")
				count += 1
			}
			XCTAssert(count == 3, "Actual count is \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	func testSelectOrderByDesc()
	{
		let db = createExampleDB(name: "testSelectOrderByDesc.sqlite3")
		do
		{
			guard let contact = try db.table(name: "contact")
				else { XCTFail("Could not get contaxct table") ; return }

			guard let id = contact.id
				else { XCTFail("Could not get id column") ; return }
			guard let name = contact.name
				else { XCTFail("Could not get name column") ; return }
			guard let familyName = contact.familyname
				else { XCTFail("Could not get family name column") ; return }
			var count = 0

			let select = Select([id, name, familyName], from: contact, where: !Filter.isNull(name), orderBy: [name.descending])
			let resultSet = try db.query(select)
			var expectedNext = ["roger", "richard", "jeremy"].makeIterator()
			for row in resultSet
			{
				let nameString: String = try row.value(column: name)
				let expected = expectedNext.next()!
				XCTAssert(nameString == expected, "Unexpected name '\(nameString)', expected '\(expected)'")
				count += 1
			}
			XCTAssert(count == 3, "Actual count is \(count)")
		}
		catch
		{
			XCTFail("\(error)")
		}
	}



	func testRetrieveVariousTypes()
	{
		let db = createExampleDB(name: "testRetrieveVariousTypes.sqlite3")
		let frankenTable = Table(name: "frankentable",
							  columns: [
								Column(name: "theKey", type: .integer, primaryKey: true, nullable: false),
								Column(name: "anInt", type: .integer),
								Column(name: "aString", type: .text),
								Column(name: "aFloat", type: .real),
								Column(name: "aBlob", type: .blob),
			])
		do
		{
			try db.create(table: frankenTable)
			let theKey = frankenTable.theKey!
			let anInt = frankenTable.anInt!
			let aString = frankenTable.aString!
			let aFloat = frankenTable.aFloat!
			let aBlob = frankenTable.aBlob!
			try db.insert(into: frankenTable, values:
				[
					theKey <- 1,
					anInt <- 1729,
					aString <- "seventeentwentynine",
					aFloat <- 17.29,
					aBlob <- Data(bytes: [1, 7, 2, 9])
				])
			try db.insert(into: frankenTable, values: [theKey <- 2]) // To give us null values

			let select = Select( [theKey, anInt, aString, aFloat, aBlob], from: frankenTable)
			for row in try db.query(select)
			{
				let key: Int = try row.value(column: theKey)
				switch key
				{
				case 1:
					let anIntValue: Int = try row.value(column: anInt)
					XCTAssert(anIntValue == 1729)
					let aStringValue: String = try row.value(column: aString)
					XCTAssert(aStringValue == "seventeentwentynine")
					let aDoubleValue: Double = try row.value(column: aFloat)
					XCTAssert(aDoubleValue == 17.29)
					let aBlobValue: Data = try row.value(column: aBlob)
					XCTAssert(aBlobValue.count == 4)
				case 2:
					let anIntValue: Int? = try row.nullableValue(column: anInt)
					XCTAssert(anIntValue == nil)
					let aStringValue: String? = try row.nullableValue(column: aString)
					XCTAssert(aStringValue == nil)
					let aDoubleValue: Double? = try row.nullableValue(column: aFloat)
					XCTAssert(aDoubleValue == nil)
					let aBlobValue: Data? = try row.nullableValue(column: aBlob)
					XCTAssert(aBlobValue == nil)
				default:
					XCTFail("Unexpected key: \(key)")
				}
			}
		}
		catch
		{
			XCTFail("\(error)")
		}
	}


	func createExampleDB(name: String) -> Connection
	{
		return SelectTests.helper.createExampleDB(name: name, populate: {
			db in
			let contact = Table(
				name: "contact",
				columns: [
					Column(name: "id", type: .integer, primaryKey: true, nullable: false),
					Column(name: "name", type: .text),
					Column(name: "familyname", type: .text),
				]
			)
			let id = contact.id!
			let name = contact.name!
			let familyName = contact.familyname!

			try db.create(table: contact)
			try db.insert(into: contact, values: [id <- 1, name <- "roger", familyName <- "waters"])
			try db.insert(into: contact, values: [id <- 2, name <- "jeremy", familyName <- "pereira"])
			try db.insert(into: contact, values: [id <- 3, name <- "richard", familyName <- "pereira"])
			try db.insert(into: contact, values: [id <- 4, familyName <- "nullboy"])
		})
	}

}
