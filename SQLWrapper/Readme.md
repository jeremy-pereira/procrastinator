# SQLWrapper

This is a library that wraps SQL implementations in a - hopefully - Swifty way.

Some principles:

- The API should be agnostic of the underlying database engine. Apart from creating the connection, nothing should be specific to a particlular database implementation.
- The code must look "Swifty" to the programmer. Ideally, they should be able to use Swift types and Swift paradigms to code with.
- The wrapper should be extensible. If you have a database engine you want to wrap, it should be relatively easy to do so.

Currently, I have only implemented support for SQLite3.

## How to Use 

First you need to create a connection, using one of the implementations. First, import `SQLWrapper` and then call the constructor of an implementation of `Connection`.

```Swift
let db = try SQLite3.open(path: "some/path/db.sqlite3")
```

### Create ###

The `Connection` object has methods on it to allow you to create tables, insert update and delete from them and to run select queries. For example, to create a table, first create `Table` object and then pass it to the connection's `create` function.

```Swift
let contact = Table(
			   name: "contact",
			columns: [
					Column(name: "id", type: .integer, primaryKey: true, nullable: false),
					Column(name: "name", type: .text),
					Column(name: "familyname", type: .text),
				]
			)
try db.create(table: contact)
```
The above is the same as running the following query (assuming `sqlite3`):

```SQL
CREATE TABLE contact (
	id INTEGER PRIMARY KEY NOT NULL, 
	name TEXT, 
	familyname TEXT)
```

For other SQL queries, you do not need to manufacture the `Table` object yourself, you can get it from the database:

```Swift
guard let contact = try db.table(name: "contact")
else { /* handle table does not exist */ }
```

### Insert ###

To insert rows into a table, use the `insert` function:

```Swift
do
{
    let contact = try db.table(name: "contact")!

    let id = contact.id!
    let name = contact.name!

	let rows = try db.insert(into: contact, values: [id <- 1, name <- "jeremy"])
}
catch
{
    // Handle errors
}
```
There are several things to note here. 

Firstly, the `Table` type allows [dynamic member lookup](https://github.com/apple/swift-evolution/blob/master/proposals/0195-dynamic-member-lookup.md) to get the columns of the table. This means we can reference the columns of a table as if they are Swift properties. Note that a particular column may not exist, so the column properties are `Optional<Column>` which is why I force unwrapped them in this example.

There is also a function `column(_ name: String) -> Column?` which is identical to the dynamic member except that it allows column names that don't adhere to Swift identifier syntax and allows you to refer to columns with the same name as normal properties of `Table`. For example, if you have a column called `tableName` you can't use dynamic member lookup because that is a real property containing a string containing the table's name.

The insert statement itself does not adhere directly to the format of a SQL `INSERT` statement in that the columns and values are not specified separately. Instead we have an array of bindings from columns to values. 

The `<-` operator creates a *binding* between a column and an instance of any type that conforms to the protocol `SQLTypeable`. An `SQLTypeable` has functions and properties that help the framework convert Swift values to values in the underlying database engine and vice versa. The framework provides extensions to `String` and `Int` and some other types that make them conform to the protocol.

The `insert` return the number of rows that were affected by the query. In the case of `insert` the answer will always be `1` because errors will always cause the function to throw an error.

### Update ###

To update rows in a database table, do the following:

```Swift

do
{
	let contact = try db.table(name: "contact")!
	let name = contact.name!
	let familyName = contact.familyname!

	let count = try db.update(contact, set: [name <- "muddy"], where: familyName == "waters")
}
catch
{
	// Handle the error
}
```

As with `insert` there is a list of bindings, which tells the framework what values to set for which columns. However, we now have a where clause to which you pass a filter that tells the framework which rows to update. The filter looks like a standard Swift boolean expression, but it isn't. It actually creates an abstract tree that the framework uses to construct the where clause of the `UPDATE` statement. The twigs of the tree are actually implicit bindings between the the column and the value to which it is being compared. 

The `update` returns a count of the rows in the table that were updated.

### Delete ###

Deleting rows works very much like updating them. The major difference is that you do not have a `set` argument.

```Swift
do
{
	let contact = try db.table(name: "contact")!
	let familyName = contact.familyname!
	let count = try db.delete(from: contact, where: familyName == "pereira")
}
catch
{
	XCTFail("\(error)")
}
```
As with update, the number of rows affected (i.e. deleted) is returned.

### Queries ###

Since select statements can get very complex in SQL, it is hard to model one as the arguments to a function. Thus, for queries, you must first create a select object that represents the SQL query statement and then call the `query` function. Here is an example:

```Swift
do
{
	let db = createExampleDB(name: "testSelectWithOr.sqlite3")

	let contact = try db.table(name: "contact")!
	let id = contact.id!
	let name = contact.name
	let familyName = contact.familyname
	var count = 0

	let select = Select([id, name, familyName], 
	                     from: contact, 
	                    where: name == "jeremy" || name == "richard")
	var resultSet = try db.query(select)
	while let row = try resultSet.nextRow()
	{
		let nameString: String = try row.value(column: name)
		// Do something with nameString
		count += 1
	}
	// count is the total number of rows processed
}
catch
{
	// Handle the error
}
```
The `where` argument works just like in `update` and `delete`. Other than that, in this select, we asked for three columns and specified the `contact` table. 

Having got the result set, we need to iterate through it. This is the job of the `while` statement in this example. The function `nextRow` returns a row from the result set, or `nil`, if we have processed all of the rows. Note that this is a throwing function because with some database engines (SQLite3, for example), the query is not executed until you try to retrieve the first row. 

You fetch the values of each column in a row by calling one of the `row.value(column:)` functions. There is a `row.value(column:)` function for each of the types that can be returned i.e. `String`, `Int`, `Double`, `Data`. This list will probably expand in the future. Because the functions differ only in their return type, you need to specify the type you are expecting, so the compiler can determine which function to use, hence why the `let` in the example explicitly states the type of the `let` constant.

If a column contains `null` values, the `row.value(column:)` function will throw an error. You can get around this by using one of the  `nullableValue(column:)` functions. These return a value wrapped in an optional e.g.

```Swift
let nameString: String? = try row.nullableValue(column: name)
```
If the value for the column is null, this function will return `nil` rather than throw.

`ResultSet` conforms to `Sequence` so you can do things like:

```Swift
for row in try db.query(select)
{
    // Do stuff
}
```

However, in this case, if there is an database error, the query won't throw because the `next()` function of a `Sequence` is not allowed to throw. Instead, `ResultSet.next()` will catch the error and record it and then return `nil`. You should probably always check the error after the sequence is finished.

```Swift
var resultSet = try db.query(select)
for row in resultSet
{
    // Do stuff
}
if let error = resultSet.error
{
	throw error
}
```

The fact that `ResultSet` is a sequence lets you use the ful power of Swift sequences with it.

```Swift
// Counts the results returned
var resultSet = try db.query(select)
let count =  resultSet.reduce(0)
{
    countSoFar, _ in countSoFar + 1
}
```

```Swift
// Gets a list of names
var resultSet = try db.query(select)
let names = resultSet.map { row -> String in row.value(column: name) }
```



