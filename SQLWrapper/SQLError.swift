//
//  SQLError.swift
//  SQLWrapper
//
//  Created by Jeremy Pereira on 03/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import Foundation

public protocol ExpressesError: Error
{

	/// Get an extended description of the error including more information than
	/// the basic error code
	var extendedDescription: String { get }
}


/// Basic common SQL errors
///
public enum SQLError: ExpressesError
{
	case abort(String)
	case cantOpen
	case dbIsNotOpen
	case invalidTableName
	case invalidColumnName
	// XXX: Might be a SQLite3 specific error
	case statementFinalised
	case invalidType
	case typeConversion(String)
	case isNull
	case unexpectedBinding

	public var extendedDescription: String { return "\(self)" }
}

extension Error
{
	public var description: String
	{
		if let extended = self as? ExpressesError
		{
			return extended.extendedDescription
		}
		else
		{
			return "\(self)"
		}
	}
}

/// Result type for things that can return a value but might also fail with
/// some sort of SQL error.
///
/// - success: returned if the function succeeds
/// - failure: returned if something went wrong.
@available(swift, deprecated: 5.0, message: "The `Swift.Result` type will appear in 5.0 and should be used instead")
public enum Result<Success>
{
	case success(Success)
	case failure(ExpressesError)
}

