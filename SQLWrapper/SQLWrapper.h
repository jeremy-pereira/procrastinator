//
//  SQLWrapper.h
//  SQLWrapper
//
//  Created by Jeremy Pereira on 02/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for SQLWrapper.
FOUNDATION_EXPORT double SQLWrapperVersionNumber;

//! Project version string for SQLWrapper.
FOUNDATION_EXPORT const unsigned char SQLWrapperVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SQLWrapper/PublicHeader.h>


