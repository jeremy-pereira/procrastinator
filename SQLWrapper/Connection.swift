//
//  Connection.swift
//  SQLWrapper
//
//  Created by Jeremy Pereira on 02/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import Foundation


/// Models a database connection.
///
/// A database connection has to be a class because it contains state including
/// probably an underlying database handle.
public protocol Connection: class
{

	/// Manually close the connection.
	///
	/// Classes conforming to `Connection` shoild arrange for the connection to
	/// be closed in `deinit` if not already closed. However, it is useful to
	/// have a means of forcing a close to stop unused resources from lying
	/// around.
	/// - Returns: An error if it goes wrong. This is done so you can ignore it
	///            and use `close()` more simply in deferred blocks.
	@discardableResult func close() -> ExpressesError?


	/// Create a new table in the database.
	///
	/// If the table is created successfully, it's bound to the database
	/// in which the table was created.
	///
	/// - Parameter table: The table to create
	/// - Throws: If we cannot create the table for any reason
	func create(table: Table) throws


	/// Insert a row into a table
	///
	/// - Parameters:
	///   - table: The table to insert into
	///   - values: a list of bindings between columns and the values to insert
	/// - Returns: The number of rows affected by the insert, probably always 1
	/// - Throws: If the insert fails
	@discardableResult func insert(into table: Table, values: [Binding]) throws -> Int

	/// Do a database update.
	///
	/// - Parameters:
	///   - table: The table to update
	///   - bindings: columns and values to set for each column
	///   - where: which rows of the table to update
	/// - Returns: The number of rows affected by the update
	/// - Throws: If the update goes wrong.
	@discardableResult func update(_ table: Table, set bindings: [Binding], where: Filter) throws -> Int

	/// Delete rows from a table.
	///
	/// - Parameters:
	///   - table: The table from which to delete rows.
	///   - where: A filter to say which rows to delete
	/// - Returns: The number of rows deleted.
	/// - Throws: If the delete goes wrong.
	@discardableResult func delete(from table: Table, where: Filter) throws -> Int

	/// Return a `Table` object that models the real table with the given name.
	///
	/// - Parameter name: Name of the table to find.
	/// - Returns: A table object or `nil` if there is no such table.
	/// - Throws: If something goes wrong trying to get the table schema, other
	///           than there being no table with the given name.
	func table(name: String) throws -> Table?

	/// Returns a sequence of rows that sre the result of matching the query
	///
	/// - Parameter select: The select to perform
	/// - Returns: The set of matching rows
	/// - Throws: if the select goes wrong in any way.
	func query(_ select: Select) throws -> ResultSet


	/// Begin an explicit database transaction
	///
	/// - Throws: if we can't begin a transaction
	func beginTransaction() throws


	/// Commiot an in progress transaction
	///
	/// - Throws: If we can't commit a transaction
	func commitTransaction() throws


	/// Rolls back a transaction
	///
	/// - Throws: if we can't roll the transaction back
	func rollBackTransaction() throws
}

extension Connection
{
	/// Run some code inside an explicit database transaction
	///
	/// If you want to explicitly bail out of a transaction, throw an error.
	/// `SQLError.abort` is defined if there is no other natural error you want
	/// to throw.
	///
	/// - Parameter block: The block to run
	/// - Throws: If the transaction fails and is rolled back for any reason
	public func withTransaction(_ block: () throws -> ()) throws
	{
		try self.beginTransaction()
		do
		{
			try block()
			try self.commitTransaction()
		}
		catch
		{
			try self.rollBackTransaction()
			throw error
		}
	}
}
