//
//  Join.swift
//  SQLWrapper
//
//  Created by Jeremy Pereira on 11/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Something that can be joined with a table tables
public protocol Joinable
{
	/// Return the inner join of this joinable and another table
	///
	/// - Parameters:
	///   - type: The type of join
	///   - table: The other table to join on
	///   - on: The join constraint
	/// - Returns: A new joinable
	func join(type: Join.Kind, table: Table, on: Filter) -> Joinable
}

extension Joinable
{
	/// Convenience function to do an uinner join
	///
	/// - Parameters:
	///   - table: The table on which to join
	///   - on: The join constraint
	/// - Returns: A Joinable modelling the join
	func innerJoin(_ table: Table, on: Filter) -> Joinable
	{
		return join(type: .inner, table: table, on: on)
	}

	/// Convenience function to do a left join
	///
	/// - Parameters:
	///   - table: Table to join with
	///   - on: Join constraint
	/// - Returns: An object that models a left join
	func leftJoin(_ table: Table, on: Filter) -> Joinable
	{
		return join(type: .left, table: table, on: on)
	}
}

/// Models a join between multiple tables
public struct Join: Joinable
{

	/// Join type
	///
	/// - inner: An inner join, only records from both tables appear
	/// - left: All records for the left table appear with nulls for the right
	///         if the constraint isn't fulfilled
	/// - outer: Not sure.
	public enum Kind
	{
		case inner
		case left
		case outer
	}

	public func join(type: Join.Kind, table: Table, on: Filter) -> Joinable
	{
		return Join(first: self, second: table, constraint: on, type: type)
	}

	public let first: Joinable
	public let second: Table
	public let constraint: Filter
	public let type: Kind
}


extension Table: Joinable
{
	public func join(type: Join.Kind, table: Table, on: Filter) -> Joinable
	{
		return Join(first: self, second: table, constraint: on, type: type)
	}
}

