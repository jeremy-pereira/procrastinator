//
//  SQLObjects.swift
//  SQLWrapper
//
//  Created by Jeremy Pereira on 03/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

/// A protocol that can be used to mark a type as being something that can be
/// converted to an SQL type.
///
public protocol SQLTypeable
{
	/// Get the value as an Int64
	///
	/// - Throws: If the value cannot be converted to Int64
	func asInt64() throws -> Int64
	/// Return the value as a string
	///
	/// - Returns: The value ass a string representation
	/// - Throws: If the value cannot be converted to a string
	func asString() throws -> String
	/// Return the value as a `Double`
	///
	/// - Returns: The value as a double representation
	/// - Throws: If the value cannot be converted to a `Double`
	func asDouble() throws -> Double

	/// Return the value as a blob of data
	///
	/// - Returns: The value as a `Data` object
	/// - Throws: If the value cannot be converted to data
	func asData() throws -> Data

	/// True if the value is null.
	var isNull: Bool { get }
}


/// A type that can be used to provide null values for bindings
public struct Null: SQLTypeable
{
	public var isNull: Bool { return true }

	public func asInt64() throws -> Int64
	{
		throw SQLError.isNull
	}

	public func asString() throws -> String
	{
		throw SQLError.isNull
	}

	public func asDouble() throws -> Double
	{
		throw SQLError.isNull
	}

	public func asData() throws -> Data
	{
		throw SQLError.isNull
	}
}

/// A null value for SQL nulls
public let null = Null()

/// Types supported by SQL
public enum SQLType
{
	case integer
	case text
	case real
	case blob
}
/// Models a SQL table.
///
/// This struct takes advantage of dynamic member lookup so you can get the
/// columns by writing `table.columnName` instead of `table.column("columnName")`
/// You will have to fall back on the latter for members of this type, however.
@dynamicMemberLookup
public struct Table
{
	/// The name of the table
	let tableName: String

	/// The columns in the table.
	let columns: [Column]

	/// Create a new table object.
	///
	/// TODO: This should error if there are duplicate column names
	/// - Parameters:
	///   - name: The name of the table
	///   - columns: The columns in the table
	public init(name: String, columns: [Column])
	{
		self.tableName = name
		self.columns = columns.map
		{
			var newColumn = $0
			newColumn.namespace = name
			return newColumn
		}
	}


	/// Return the column with the givewn name
	///
	/// - Parameter name: Name of the column to find
	/// - Returns: The column with the given name or `nil` if no such column exists
	public func column(_ name: String) -> Column?
	{
		return columns.first { $0.name == name }
	}

	public subscript(dynamicMember member: String) -> Column?
	{
		return column(member)
	}
}


/// Models a database table column
public struct Column
{
	/// The namespace of the column
	public fileprivate(set) var namespace: String?
	/// The name of the column
	public var name: String
	/// The tyoe of the column
	public let type: SQLType
	/// True if the column is meant to be the primary key
	public let primaryKey: Bool
	/// True if values in the column can be null
	public let nullable: Bool
	/// True if each value of the column must be unique
	public let unique: Bool

	public var qualifiedName: String
	{
		if let namespace = namespace
		{
			return namespace + "." + name
		}
		else
		{
			return name
		}
	}

	/// Crearte a table column
	///
	/// - Parameters:
	///   - name: Name of the column
	///   - type: Database type of the column
	///   - primaryKey: Is this the primary key (defaults to no)
	///   - nullable: Is this a nullable column (defaults to yes)
	///   - unique: Are values in the column constrained to be uniwue (defaults to no)
	public init(name: String, type: SQLType, primaryKey: Bool = false, nullable: Bool = true, unique: Bool = false)
	{
		self.name = name
		self.type = type
		self.primaryKey = primaryKey
		self.nullable = nullable
		self.unique = unique
	}
}

extension Column: Orderable
{
	public var orderName: String { return self.qualifiedName }

	public var isAscending: Bool { return true }

	/// Return an orderable of this column in ascending order
	public var ascending: Orderable { return self }
	/// Return an oderable of this column in descending order
	public var descending: Orderable { return Ordering(self, isAscending: false) }
}
/// Models a binding between a column and a value
public typealias Binding = (Column, SQLTypeable)

infix operator <- : AssignmentPrecedence


/// Crerates a binding for a column
///
/// - Parameters:
///   - lhs: The column to bind
///   - rhs: The value to bind to trhe column
/// - Returns: A binding between the column and the value
public func <- (lhs: Column, rhs: SQLTypeable) -> Binding
{
	return (lhs, rhs)
}


/// Describes a row result from a query
public protocol Row
{
	/// Find the value as a string for the given column
	///
	/// - Parameter column: The column for which to get the value
	/// - Returns: The string value of the column
	/// - Throws: if the column cannot be converted to an string
	func value(column: Column) throws -> String
	/// Find the value as an int for the given column
	///
	/// - Parameter column: The column for which to get the value
	/// - Returns: The int value of the column
	/// - Throws: if the column cannot be converted to an int
	func value(column: Column) throws -> Int
	/// Find the value as a `Double` for the given column
	///
	/// - Parameter column: The column for which to get the value
	/// - Returns: The double value of the column
	/// - Throws: if the column cannot be converted to an double
	func value(column: Column) throws -> Double
	/// Find the value as a `Data` for the given column
	///
	/// - Parameter column: The column for which to get the value
	/// - Returns: The data value of the column
	/// - Throws: if the column cannot be converted to a data
	func value(column: Column) throws -> Data

	/// Find the value as a string for the given column that may be nullable
	///
	/// - Parameter column: The column for which to get the value
	/// - Returns: An optional string containing the value
	/// - Throws: if the value cannot be converted to an optional string
	func nullableValue(column: Column) throws -> String?
	/// Find the value as an `Int` for the given column that may be nullable
	///
	/// - Parameter column: The column for which to get the value
	/// - Returns: An optional `Int` containing the value
	/// - Throws: if the value cannot be converted to an optional `Int`
	func nullableValue(column: Column) throws -> Int?
	/// Find the value as a `Double` for the given column that may be nullable
	///
	/// - Parameter column: The column for which to get the value
	/// - Returns: An optional `Double` containing the value
	/// - Throws: if the value cannot be converted to an optional `Double`
	func nullableValue(column: Column) throws -> Double?
	/// Find the value as a `Data` for the given column that may be nullable
	///
	/// - Parameter column: The column for which to get the value
	/// - Returns: An optional `Data` containing the value
	/// - Throws: if the value cannot be converted to an optional `Data`
	func nullableValue(column: Column) throws -> Data?
}

/// Models a set of results from a query.
///
/// This allows the implementation to use an implementation specific type for
/// results. It's designed to be wrapped in a ResultSet which conforms to
/// `Sequence` so it can be used in `for ... in`
public protocol ResultSource
{
	/// Get the next row of results
	///
	/// - Returns: The next row of results or nil if there are none left
	/// - Throws: If some other error occurs.
	mutating func nextRow() throws -> Row?
}


/// A sequence of results from a query.
///
public struct ResultSet: ResultSource, Sequence
{
	private var realSource: ResultSource

	/// Last error that occurred while using `next()`
	///
	/// If getting the next row as part of a sequence throws an error, this
	/// property will be set to the error. This us allows us to use a non
	/// throwing `next()` to conform to `IteratorProtocol` and hence `Sequence`
	public fileprivate(set) var error: Error?

	init(realSource: ResultSource)
	{
		self.realSource = realSource
	}

	/// Return the next result row.
	///
	/// This method does not set the `error` property if it throws.
	/// - Returns: The next row or `nil` if there isn't one.
	/// - Throws: if there is an error when getting a row.
	public mutating func nextRow() throws -> Row?
	{
		return try realSource.nextRow()
	}
}

extension ResultSet: IteratorProtocol
{
	public mutating func next() -> Row?
	{
		do
		{
			return try nextRow()
		}
		catch
		{
			self.error = error
			return nil
		}
	}
}
