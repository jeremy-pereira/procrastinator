//
//  SQLite3.swift
//  SQLWrapper
//
//  Created by Jeremy Pereira on 02/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
import SQLite3

private let SQLITE_TRANSIENT = unsafeBitCast(-1, to: sqlite3_destructor_type.self)

/// A class hat models a connection to an SQLite3 database
public class SQLite3
{

	let path: String

	private var dbHandle: OpaquePointer?


	/// Create a database connection object for SQLite3
	///
	/// - Parameter path: The location of the database file.
	fileprivate init(path: String, dbHandle: OpaquePointer)
	{
		self.path = path
		self.dbHandle = dbHandle
	}

	deinit
	{
		if let dbHandle = dbHandle
		{
			_ = sqlite3_close(dbHandle)
		}
	}

	/// Open a connection to an SQLite3 database
	///
	/// - Parameter path: The path to the SQLit3 file.
	/// - Returns: A `Connection` object representing the databaase connection
	/// - Throws: If there is a problem opening the connection.
	public static func open(path: String) throws -> Connection
	{
		var dbHandle: OpaquePointer?
		let ret = path.withCString
		{
			utf8String in
			return sqlite3_open(utf8String, &dbHandle)
		}
		guard ret == SQLITE_OK
		else
		{
			throw SQLite3.Error(code: ret)
		}
		return SQLite3(path: path, dbHandle: dbHandle!)
	}

	private func render(column: Column) -> String
	{
		// TODO: Support defaults
		return "\(column.name) \(render(type: column.type))\(column.primaryKey ? " PRIMARY KEY" : "")\(column.nullable ? "" : " NOT NULL")\(column.unique ? " UNIQUE" : "")"
	}

	private func render(type: SQLType) -> String
	{
		switch type
		{

		case .integer:
			return "INTEGER"
		case .text:
			return "TEXT"
		case .real:
			return "REAL"
		case .blob:
			return "BLOB"
		}
	}

	public func render(join joinable: Joinable) throws -> String
	{
		if let table = joinable as? Table
		{
			return table.tableName
		}
		else if let join = joinable as? Join
		{
			let left = try render(join: join.first)
			let right = join.second.tableName
			let joinType: String
			switch join.type
			{
			case .inner:
				joinType = "INNER JOIN"
			case .left:
				joinType = "LEFT JOIN"
			case .outer:
				joinType = "OUTER JOIN"
			}
			var bindings: [Binding] = []
			let constraint = render(filter: join.constraint, bindValues: &bindings)
			guard bindings.count == 0 else { throw SQLError.unexpectedBinding }
			return "\(left) \(joinType) \(right) ON \(constraint)"
		}
		else
		{
			fatalError("Unimplemented joinable type")
		}
	}
	/// Render a select statement as SQL
	///
	/// Note that the function assumes all values in the filter are to be bound
	/// by the prepared statement.
	/// - Parameters:
	///   - select: The select statement object
	///   - bindValues: An array in which values to bind to a prepared statement go
	/// - Returns: A string with the select statement in
	/// - Throws: if something goes wrong
	public func render(select: Select, bindValues: inout [Binding]) throws -> String
	{
		let selectColumns = select.columns.map { $0.qualifiedName }
		let sourceString: String
		if let source = select.source
		{
			sourceString = "FROM \(try render(join: source))"
		}
		else
		{
			sourceString = ""
		}
		let filterString: String
		if let filter = select.filter
		{
			filterString = " WHERE " + render(filter: filter, bindValues: &bindValues)
		}
		else
		{
			filterString = ""
		}
		let orderByString: String
		if select.order.count > 0
		{
			let orderThings = select.order.map
			{
				$0.orderName + " " + ($0.isAscending ? "ASC" : "DESC")
			}
			orderByString = " ORDER BY " + orderThings.makeList(separator: ",")
		}
		else
		{
			orderByString = ""
		}

		return "SELECT\(select.distinct ? " DISTINCT" : " ") \(selectColumns.makeList(separator: ",")) \(sourceString)\(filterString)\(orderByString)"
	}

	/// Render a filter as a string suitable for a `WHERE` clause.
	///
	/// - Parameters:
	///   - filter: The filter to render
	///   - bindValues: The bind values for comparisons in the filter. This is
	///                 an `inout` so that the bindings can be ordered with
	///                 respect to previous bindings. e.g. `UPDATE` also needs
	///                 bindings for the `SET` clause.
	/// - Returns: The rendered filter.
	public func render(filter: Filter, bindValues: inout [Binding]) -> String
	{
		switch filter
		{
		case .equals(let column, let value):
			bindValues.append((column, value))
			return "\(column.qualifiedName) = ?"
		case .isNull(let column):
			return "\(column.qualifiedName) IS NULL"
		case .or(let left, let right):
			return "(\(render(filter: left, bindValues: &bindValues))) OR (\(render(filter: right, bindValues: &bindValues)))"
		case .and(let left, let right):
			return "(\(render(filter: left, bindValues: &bindValues))) AND (\(render(filter: right, bindValues: &bindValues)))"
		case .isNotNull(let column):
			return "\(column.qualifiedName) IS NOT NULL"
		case .not(let expression):
			return "NOT (\(render(filter: expression, bindValues: &bindValues)))"
		case .joinEquals(let c1, let c2):
			return "\(c1.qualifiedName) = \(c2.qualifiedName)"
		}
	}

	/// Render the set clause of an update statement.
	///
	/// - Parameters:
	///   - set: List of bindings to go into the set clause.
	///   - bindValues: output binding list to be used by the prepared statement
	///                 This is an `inout` in case there are previous bindings
	///                 to apply.
	/// - Returns: A string representing the set clause without the keyword `SET`
	public func render(set: [Binding], bindValues: inout [Binding]) -> String
	{
		return set.map
		{
			bindValues.append($0)
			return $0.0.name + " = ?"
		}.makeList(separator: ",")
	}
}

extension SQLite3
{
	fileprivate struct Error: ExpressesError, CustomStringConvertible
	{
		let code: Int
		let extendedCode: Int
		let message: String
		let text: String

		var description: String
		{
			return "\(code)(\(extendedCode)) '\(text)' : '\(message)'"
		}

		var extendedDescription: String
		{
			return "\(code)(\(extendedCode)) '\(text)' : '\(message)'"
		}

		fileprivate init(code: Int32, dbHandle: OpaquePointer? = nil)
		{
			self.code = Int(code)
			self.text = String(cString: sqlite3_errstr(code))
			if let dbHandle = dbHandle
			{
				self.extendedCode = Int(sqlite3_extended_errcode(dbHandle))
				self.message = String(cString: sqlite3_errmsg(dbHandle))
			}
			else
			{
				self.extendedCode = 0
				self.message = ""
			}
		}
	}
}
// MARK: Connection implementation
extension SQLite3: Connection
{
	/// Close the database connection.
	///
	/// This function is called in `deinit`.  It probably should not be called
	/// from anywhere else.
	///
	/// - Returns: Returns an error if the close fails in some way.
	public func close() -> ExpressesError?
	{
		guard let dbHandle = dbHandle else { return SQLError.dbIsNotOpen }
		let sqlCode = sqlite3_close(dbHandle)
		guard sqlCode == SQLITE_OK
			else
		{
			return SQLite3.Error(code: sqlCode, dbHandle: dbHandle)
		}
		self.dbHandle = nil
		return nil
	}

	public func create(table: Table) throws
	{
		let columnStrings = table.columns.map
		{
			self.render(column: $0)
			}.makeList(separator: ",")

		let queryString = "CREATE TABLE \(table.tableName) (\(columnStrings))"
		print(queryString)
		let  preparedStatement = try prepare(query: queryString)
		_ = try preparedStatement.step()
	}

	public func insert(into table: Table, values: [Binding]) throws -> Int
	{
		let columnNames = values.map { $0.0.name }
		let questionMarks = Array<String>(repeating: "?", count: columnNames.count)
		let query = "INSERT INTO \(table.tableName) (\(columnNames.makeList(separator: ","))) VALUES(\(questionMarks.makeList(separator: ",")))"
		print(query)
		let preparedStatement = try prepare(query: query)
		for (index, binding) in values.enumerated()
		{
			let (column, value) = binding
			try preparedStatement.bind(index: index + 1, type: column.type, value: value)
		}
		_ = try preparedStatement.step()
		let rowCount = sqlite3_changes(dbHandle!)
		return Int(rowCount)
	}

	public func update(_ table: Table, set bindings: [Binding], where filter: Filter) throws -> Int
	{
		var statementBindings: [Binding] = []
		let setString = render(set: bindings, bindValues: &statementBindings)
		let filterString = render(filter: filter, bindValues: &statementBindings)
		let queryString = "UPDATE \(table.tableName) SET \(setString) WHERE \(filterString)"
		print(queryString)
		let preparedStatement = try prepare(query: queryString)
		for (index, binding) in statementBindings.enumerated()
		{
			let (column, value) = binding
			try preparedStatement.bind(index: index + 1, type: column.type, value: value)
		}
		_ = try preparedStatement.step()
		let rowCount = sqlite3_changes(dbHandle!)
		return Int(rowCount)
	}

	public func delete(from table: Table, where filter: Filter) throws -> Int
	{
		var statementBindings: [Binding] = []
		let filterString = render(filter: filter, bindValues: &statementBindings)
		let queryString = "DELETE FROM \(table.tableName) WHERE \(filterString)"
		print(queryString)
		let preparedStatement = try prepare(query: queryString)
		for (index, binding) in statementBindings.enumerated()
		{
			let (column, value) = binding
			try preparedStatement.bind(index: index + 1, type: column.type, value: value)
		}
		_ = try preparedStatement.step()
		let rowCount = sqlite3_changes(dbHandle!)
		return Int(rowCount)
	}

	public func query(_ select: Select) throws -> ResultSet
	{
		var bindArgs: [Binding] = []
		let queryString = try render(select: select, bindValues: &bindArgs)
		print(queryString)
		let preparedStatement = try prepare(query: queryString, resultColumns: select.columns)
		for (i, binding) in bindArgs.enumerated()
		{
			let (column, value) = binding
			try preparedStatement.bind(index: i + 1, type: column.type, value: value)
		}
		return try ResultSet(realSource: QueryResult(statement: preparedStatement))
	}

	public func table(name: String) throws -> Table?
	{
		guard !name.contains("'") else { throw SQLError.invalidTableName }

		let queryString = "PRAGMA table_info('\(name)')"
		let preparedStatement = try prepare(query: queryString)
		var columns: [Column] = []

		while let row = try preparedStatement.step()
		{
			let column = Column(name: row.columnAsString(1),
								type: try SQLType.from(sqlite3Type: row.columnAsString(2)),
								primaryKey: row.columnAsInt32(5) != 0,
								nullable: row.columnAsInt32(3) != 0,
								unique: false)
			columns.append(column)
		}
		guard columns.count > 0 else { return nil }
		return Table(name: name, columns: columns)
	}

	public func beginTransaction() throws
	{
		let statement = try prepare(query: "BEGIN TRANSACTION")
		_ = try statement.step()
	}

	public func commitTransaction() throws
	{
		let statement = try prepare(query: "COMMIT TRANSACTION")
		_ = try statement.step()
	}

	public func rollBackTransaction() throws
	{
		let statement = try prepare(query: "ROLLBACK TRANSACTION")
		_ = try statement.step()
	}
}

// MARK: Prepared statements
extension SQLite3
{

	fileprivate func prepare(query: String, resultColumns: [Column] = []) throws -> PreparedStatement
	{
		guard let dbHandle = dbHandle else { throw SQLError.dbIsNotOpen }

		var preparedStatement: OpaquePointer?
		let sqlResult = sqlite3_prepare_v2(dbHandle, query, -1, &preparedStatement, nil)
		guard sqlResult == SQLITE_OK
			else { throw SQLite3.Error(code: sqlResult, dbHandle: dbHandle) }
		return PreparedStatement(preparedStatement!, connection: self, columns: resultColumns)
	}

	fileprivate class PreparedStatement
	{
		private var connection: SQLite3
		fileprivate private(set) var sqlite3Prepared: OpaquePointer?
		fileprivate let columns: [Column]

		fileprivate init(_ sqlite3Prepared: OpaquePointer, connection: SQLite3, columns: [Column] = [])
		{
			self.sqlite3Prepared = sqlite3Prepared
			self.connection = connection
			self.columns = columns
		}

		deinit
		{
			_ = self.finalise()
		}

		fileprivate func finalise() -> ExpressesError?
		{
			if let sqlite3Prepared = sqlite3Prepared
			{
				let sqlCode = sqlite3_finalize(sqlite3Prepared)
				self.sqlite3Prepared = nil
				if sqlCode != SQLITE_OK
				{
					return Error(code: sqlCode, dbHandle: connection.dbHandle)
				}
			}
			return nil
		}

		fileprivate func bind(index: Int, type: SQLType, value: SQLTypeable) throws
		{
			guard let sqlite3Prepared = sqlite3Prepared
				else { throw SQLError.statementFinalised }
			let sqlCode: Int32
			if value.isNull
			{
				sqlCode = sqlite3_bind_null(sqlite3Prepared, Int32(index))
			}
			else
			{
				switch type
				{
				case .integer:
					sqlCode = sqlite3_bind_int64(sqlite3Prepared, Int32(index), try value.asInt64())
				case .text:
					sqlCode = sqlite3_bind_text(sqlite3Prepared, Int32(index), try value.asString(), -1, SQLITE_TRANSIENT)
				case .real:
					sqlCode = sqlite3_bind_double(sqlite3Prepared, Int32(index), try value.asDouble())
				case .blob:
					let data = try value.asData()
					sqlCode = data.withUnsafeBytes
					{
						return sqlite3_bind_blob(sqlite3Prepared, Int32(index), $0, Int32(data.count), SQLITE_TRANSIENT)
					}
				}
			}
			guard sqlCode == SQLITE_OK
				else { throw SQLite3.Error(code: sqlCode, dbHandle: connection.dbHandle) }
		}
		/// Evaluate the prepared statement.
		///
		/// - Returns: A row if the evaluation results in a row of data, nil if not
		/// - Throws: if the connection is not open or other issue
		fileprivate func step() throws -> SQLite3Row?
		{
			guard let dbStatement = self.sqlite3Prepared else { throw SQLError.statementFinalised }
			let sqlResult = sqlite3_step(dbStatement)
			switch sqlResult
			{
			case SQLITE_DONE:
				return nil
			case SQLITE_ROW:
				return SQLite3Row(statement: self)
			default:
				throw Error(code: sqlResult, dbHandle: self.connection.dbHandle)
			}
		}
	}

	public struct SQLite3Row: Row
	{
		fileprivate let statement: PreparedStatement

		func columnAsString(_ index: Int) -> String
		{
			let cStringPointer = sqlite3_column_text(statement.sqlite3Prepared!, Int32(index))
			return String(cString: cStringPointer!)
		}

		func columnAsInt32(_ index: Int) -> Int32
		{
			return sqlite3_column_int(statement.sqlite3Prepared!, Int32(index))
		}

		func columnAsInt64(_ index: Int) -> Int64
		{
			return sqlite3_column_int64(statement.sqlite3Prepared!, Int32(index))
		}

		func columnAsData(_ index: Int) -> Data
		{
			let sql3Index = Int32(index)
			guard let dbStatement = statement.sqlite3Prepared
			else
			{
				fatalError("Managed to try to receive data from a finalised statement")
			}
			let byteCount = sqlite3_column_bytes(dbStatement, sql3Index)
			guard byteCount > 0 else { return Data() }

			guard let pointer = sqlite3_column_blob(dbStatement, sql3Index)
				else { fatalError ("Incorrectly assumed if byteCount > 0, pointer cannot be null") }
			let ret = Data(bytes: pointer, count: Int(byteCount))
			return ret
		}

		func columnAsDouble(_ index: Int) -> Double
		{
			return sqlite3_column_double(statement.sqlite3Prepared!, Int32(index))
		}

		public func value(column: Column) throws -> String
		{
			guard let index = statement.columns.firstIndex(where: { $0.qualifiedName == column.qualifiedName })
			else { throw SQLError.invalidColumnName }

			guard sqlite3_column_type(statement.sqlite3Prepared!, Int32(index)) != SQLITE_NULL
				else { throw SQLError.isNull }
			return columnAsString(index)
		}

		public func value(column: Column) throws -> Int
		{
			guard let index = statement.columns.firstIndex(where: { $0.qualifiedName == column.qualifiedName })
				else { throw SQLError.invalidColumnName }

			guard sqlite3_column_type(statement.sqlite3Prepared!, Int32(index)) != SQLITE_NULL
				else { throw SQLError.isNull }
			return Int(columnAsInt64(index))
		}

		public func value(column: Column) throws -> Double
		{
			guard let index = statement.columns.firstIndex(where: { $0.qualifiedName == column.qualifiedName })
				else { throw SQLError.invalidColumnName }

			guard sqlite3_column_type(statement.sqlite3Prepared!, Int32(index)) != SQLITE_NULL
				else { throw SQLError.isNull }
			return columnAsDouble(index)
		}

		public func value(column: Column) throws -> Data
		{
			guard let index = statement.columns.firstIndex(where: { $0.qualifiedName == column.qualifiedName })
				else { throw SQLError.invalidColumnName }

			guard sqlite3_column_type(statement.sqlite3Prepared!, Int32(index)) != SQLITE_NULL
				else { throw SQLError.isNull }
			return columnAsData(index)
		}

		public func nullableValue(column: Column) throws -> String?
		{
			guard let index = statement.columns.firstIndex(where: { $0.qualifiedName == column.qualifiedName })
				else { throw SQLError.invalidColumnName }

			guard sqlite3_column_type(statement.sqlite3Prepared!, Int32(index)) != SQLITE_NULL
				else { return nil }
			return try value(column: column)
		}

		public func nullableValue(column: Column) throws -> Int?
		{
			guard let index = statement.columns.firstIndex(where: { $0.qualifiedName == column.qualifiedName })
				else { throw SQLError.invalidColumnName }

			guard sqlite3_column_type(statement.sqlite3Prepared!, Int32(index)) != SQLITE_NULL
				else { return nil }
			return try value(column: column)
		}

		public func nullableValue(column: Column) throws -> Double?
		{
			guard let index = statement.columns.firstIndex(where: { $0.qualifiedName == column.qualifiedName })
				else { throw SQLError.invalidColumnName }

			guard sqlite3_column_type(statement.sqlite3Prepared!, Int32(index)) != SQLITE_NULL
				else { return nil }
			return try value(column: column)
		}

		public func nullableValue(column: Column) throws -> Data?
		{
			guard let index = statement.columns.firstIndex(where: { $0.qualifiedName == column.qualifiedName })
				else { throw SQLError.invalidColumnName }

			guard sqlite3_column_type(statement.sqlite3Prepared!, Int32(index)) != SQLITE_NULL
				else { return nil }
			return try value(column: column)
		}

	}

	fileprivate struct QueryResult: ResultSource
	{
		private let statement: PreparedStatement

		init(statement: PreparedStatement) throws
		{
			self.statement = statement
		}

		mutating func nextRow() throws -> Row?
		{
			return try statement.step()
		}
	}
}

extension SQLType
{
	static func from(sqlite3Type: String) throws -> SQLType
	{
		switch sqlite3Type
		{
		case "INTEGER":
			return .integer
		case "TEXT":
			return .text
		case "BLOB":
			return .blob
		case "REAL":
			return .real
		default:
			throw SQLError.invalidType
		}
	}
}
