//
//  Extensions.swift
//  SQLWrapper
//
//  Created by Jeremy Pereira on 04/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

extension Sequence where Element == String
{
	/// Make a string list of strings using the given separator between elements
	///
	/// - Parameter separator: The string that separates elements of the list
	/// - Returns: A list of the strings as a string
	public func makeList(separator: String) -> String
	{
		var first: Bool = true
		return self.reduce("",
		{
			stringSoFar, thisElement in
			if first
			{
				first = false
				return stringSoFar + thisElement
			}
			else
			{
				return stringSoFar + separator + thisElement
			}
		})
	}
}


extension Int: SQLTypeable
{
	public var isNull: Bool { return false }

	public func asInt64() -> Int64
	{
		return Int64(self)
	}

	public func asString() -> String
	{
		return "\(self)"
	}

	public func asDouble() -> Double
	{
		return Double(self)
	}

	public func asData() -> Data
	{
		var anInt = self
		return  Data(bytes: &anInt, count: MemoryLayout.size(ofValue: anInt))
	}

}
extension String: SQLTypeable
{
	public var isNull: Bool { return false }

	public func asInt64() throws -> Int64
	{
		guard let ret = Int64(self)
			else { throw SQLError.typeConversion("String -> Int64") }
		return ret
	}

	public func asString() -> String
	{
		return self
	}

	public func asDouble() throws -> Double
	{
		guard let ret = Double(self)
			else { throw SQLError.typeConversion("String -> Double") }
		return ret
	}

	public func asData() throws -> Data
	{
		guard let data = self.data(using: .utf8)
			else { throw SQLError.typeConversion("String -> Data (utf8)") }
		return  data
	}
}

extension Optional: SQLTypeable where Wrapped: SQLTypeable
{
	public var isNull: Bool
	{
		switch self
		{
		case .none:
			return true
		case .some:
			return false
		}
	}

	public func asInt64() throws -> Int64
	{
		switch self
		{
		case .none:
			throw SQLError.isNull
		case .some(let value):
			return try value.asInt64()
		}
	}

	public func asString() throws -> String
	{
		switch self
		{
		case .none:
			throw SQLError.isNull
		case .some(let value):
			return try value.asString()
		}
	}

	public func asDouble() throws -> Double
	{
		switch self
		{
		case .none:
			throw SQLError.isNull
		case .some(let value):
			return try value.asDouble()
		}
	}

	public func asData() throws -> Data
	{
		switch self
		{
		case .none:
			throw SQLError.isNull
		case .some(let value):
			return try value.asData()
		}
	}
}

extension Double: SQLTypeable
{
	public func asInt64() throws -> Int64
	{
		guard let ret = Int64(exactly: self)
			else { throw SQLError.typeConversion("Double -> Int64") }
		return ret
	}

	public func asString() throws -> String
	{
		return "\(self)"
	}

	public func asDouble() throws -> Double
	{
		return self
	}

	public func asData() throws -> Data
	{
		var aDouble = self
		return  Data(bytes: &aDouble, count: MemoryLayout.size(ofValue: aDouble))

	}

	public var isNull: Bool { return false }
}

extension Data: SQLTypeable
{
	public func asInt64() throws -> Int64
	{
		throw SQLError.typeConversion("Data -> Int64")
	}

	public func asString() throws -> String
	{
		guard let ret = String(bytes: self, encoding: .utf8)
			else { throw SQLError.typeConversion("Data -> String (assumed utf8 encodoing)") }
		return ret
	}

	public func asDouble() throws -> Double
	{
		throw SQLError.typeConversion("Data -> Double")
	}

	public func asData() throws -> Data
	{
		return self
	}

	public var isNull: Bool { return false }
}

