//
//  Select.swift
//  SQLWrapper
//
//  Created by Jeremy Pereira on 07/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

/// Models the filter used in a where clause
public indirect enum Filter
{
	case equals(Column, SQLTypeable)
	case isNull(Column)
	case isNotNull(Column)
	case or(Filter, Filter)
	case and(Filter, Filter)
	case not(Filter)
	case joinEquals(Column, Column)


	/// Creates a filter consisting of the union of the results of the two
	/// arguments.
	///
	/// - Parameters:
	///   - lhs: A filter
	///   - rhs: Another filter
	/// - Returns: A filter representing the `OR` of the argument filters.
	public static func || (lhs: Filter, rhs: Filter) -> Filter
	{
		return Filter.or(lhs, rhs)
	}

	/// Creates a filter consisting of the intersection of the results of the two
	/// arguments.
	///
	/// - Parameters:
	///   - lhs: A filter
	///   - rhs: Another filter
	/// - Returns: A filter representing the `AND` of the argument filters.
	public static func && (lhs: Filter, rhs: Filter) -> Filter
	{
		return Filter.and(lhs, rhs)
	}


	/// The not operator
	///
	/// Note that `.isNull(column)` is not converted to `.not(.isNull(column))`
	/// but to `.isNotNull(column)` because I have a feeling that SQL syntax
	/// does not like `NOT (column IS NULL)`
	/// - Parameter expression: Expression to "not"
	/// - Returns: The same expression "not"ed
	public static prefix func !(expression: Filter) -> Filter
	{
		switch expression
		{
		case .isNull(let column):
			return .isNotNull(column)
		default:
			return .not(expression)
		}
	}
}

/// Create a filter representing the results where the value of the column
/// is equal to the value
///
/// - Parameters:
///   - lhs: A column
///   - rhs: A value
/// - Returns: A filter reresenting the equality test
public func == (lhs: Column, rhs: SQLTypeable) -> Filter
{
	return Filter.equals(lhs, rhs)
}

/// Create a filter representing equality between two columns
///
/// - Parameters:
///   - lhs: one column
///   - rhs: another column
/// - Returns: equality of the two columns
public func == (lhs: Column, rhs: Column) -> Filter
{
	return Filter.joinEquals(lhs, rhs)
}
/// Models a select statement
public struct Select
{
	/// If true, only distinct results are returned
	public let distinct: Bool
	/// Columns to select from the table
	///
	/// TODO: We will need to generalise this to allow SQL functions
	public let columns: [Column]

	/// The source of the select - a table or a join between two tables
	///
	public let source: Joinable?

	/// The filter used in a where clause
	public let filter: Filter?

	/// Models the sort order.
	///
	/// an empty array indicates no order by clause
	public let order: [Orderable]

	/// What columns do we group by
	///
	/// An empty array indicates no grouping
	public let grouping: [Column]

	public init(distinct: Bool = false, _ columns: [Column], from: Joinable? = nil, where filter: Filter? = nil, groupBy: [Column] = [], orderBy: [Orderable] = [])
	{
		self.distinct = distinct
		self.columns = columns
		self.source = from
		self.filter = filter
		self.grouping = groupBy
		self.order = orderBy
	}
}


/// Protocol defining objects that can be used to order a query
public protocol Orderable
{
	/// The name of the thing doing the ordering
	var orderName: String { get }
	/// True if the ordering is ascending
	var isAscending: Bool { get }
}


internal struct Ordering: Orderable
{
	private let underlying: Orderable
	public let isAscending: Bool
	public var orderName: String { return underlying.orderName }

	init(_ underlying: Orderable, isAscending: Bool)
	{
		self.underlying = underlying
		self.isAscending = isAscending
	}
}
