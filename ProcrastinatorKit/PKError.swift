//
//  PKError.swift
//  ProcrastinatorKit
//
//  Created by Jeremy Pereira on 14/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Errors thrown from this module
///
/// - invalidDatabase: The database at the given path is not a `Procrastinator`
///                    database
/// - unsupportedVersion: The database is at a version we can't handle.
public enum PKError: Error
{
	case invalidDatabase(String)
	case unsupportedVersion(Int)
}
