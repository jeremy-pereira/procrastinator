//
//  Database.swift
//  ProcrastinatorKit
//
//  Created by Jeremy Pereira on 14/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
import SQLWrapper

/// Models a database of tasks and time lines etc
public class Database
{
	public static let expectedVersion = 1

	/// The path to this particular database
	public let path: String

	private let connection: Connection

	/// The schema version of this database
	public private(set) var version = -1

	/// Create or open a connection to the database
	///
	/// - Parameter path: The path at which the database resides
	/// - Throws: If we cannot open the database
	init(path: String) throws
	{
		self.path = path
		connection = try SQLite3.open(path: path)
		try loadConfig()
	}

	private func loadConfig() throws
	{
		if try connection.table(name: "config") == nil
		{
			try createNewTables()
		}
		let config = try connection.table(name: "config")!
		guard let version = config.version else { throw PKError.invalidDatabase(path) }
		guard let id = config.id else { throw PKError.invalidDatabase(path) }

		let select = Select([version], from: config, where: id == 1)
		var result = try connection.query(select)
		guard let row = try result.nextRow() else { throw PKError.invalidDatabase(path) }
		let versionNumber: Int = try row.value(column: version)
		// TODO: We will want to support upgrading the database from old versions
		guard versionNumber == Database.expectedVersion else { throw PKError.unsupportedVersion(versionNumber) }
		self.version = versionNumber
	}

	func createNewTables() throws
	{
		let config = Table(name: "config", columns:
			[
				Column(name: "id", type: .integer, primaryKey: true, nullable: false, unique: true),
				Column(name: "version", type: .integer, primaryKey: false, nullable: false, unique: true),
			])
		let timeline = Table(name: "timeline", columns:
			[
				Column(name: "id", type: .integer, primaryKey: true, nullable: false, unique: true),
			])
		let recipe = Table(name: "recipe", columns:
			[
				Column(name: "id", type: .integer, primaryKey: true, nullable: false, unique: true),
			])
		let task = Table(name: "task", columns:
			[
				Column(name: "id", type: .integer, primaryKey: true, nullable: false, unique: true),
				Column(name: "shortDescription", type: .text, nullable: false),
				Column(name: "duration", type: .real),
			])
		try connection.withTransaction
		{
			try connection.create(table: config)
			try connection.insert(into: config, values: [config.id! <- 1, config.version! <- Database.expectedVersion])
			try connection.create(table: timeline)
			try connection.create(table: task)
			try connection.create(table: recipe)
		}
	}
}
