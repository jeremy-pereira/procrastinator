//
//  Task.swift
//  ProcrastinatorKit
//
//  Created by Jeremy Pereira on 02/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
import Foundation

/// A unit of work
public class Task
{
	public private(set) var shortDescription: String
	public var duration: TimeInterval = 0

	public init(_ shortDescription: String)
	{
		self.shortDescription = shortDescription
	}
}
