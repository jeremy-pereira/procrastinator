//
//  ProcrastinatorKit.h
//  ProcrastinatorKit
//
//  Created by Jeremy Pereira on 01/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for ProcrastinatorKit.
FOUNDATION_EXPORT double ProcrastinatorKitVersionNumber;

//! Project version string for ProcrastinatorKit.
FOUNDATION_EXPORT const unsigned char ProcrastinatorKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ProcrastinatorKit/PublicHeader.h>


