//
//  TimeLine.swift
//  ProcrastinatorKit
//
//  Created by Jeremy Pereira on 01/03/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

/// Protocol adopted by objects that can be scheduled into a time line
public protocol Schedulable
{

}

/// Represents a person's whole time line
public class TimeLine
{
	public func add(_ item: Schedulable, endingAt: Date)
	{
		
	}
}
